<!-- Index. principal -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-4 col-sm-3 col-xs-6'); ?>>
	<div class="blog-item-wrap square">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
			<div class="thumb-img" style="background-image:url(
            	<?php if (get_post_thumbnail_id()) {
                	echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                } else {
                    echo bloginfo('template_directory') . '/img/default-thumb.png';
                } ?>);">
            </div>
			<h2 class="entry-title">
				<?php the_title(); ?>
			</h2><!-- .entry-title -->
		</a>
	</div><!-- square -->

	<div class="post-inner-content">
		<header class="entry-header page-header">

				<?php if ( 'post' == get_post_type() ) : ?>
					<div class="entry-meta row">
						<div class="post_date col-md-6 text-left">
							<span class="fa fa-calendar"></span>
							<?php the_time('d/m/Y') ?>
						</div>
						<div class="post_categories col-md-6 text-right">
							<span class="fa fa-tags"></span>

							<?php
								$category = get_the_category();
								if ($category) {
								  echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "Ver todas las entradas de %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
								}
							?>

						</div>
						<?php edit_post_link( __( 'Editar', 'AlpheratzTheme' ), '<div class="edit-post"><span class="glyphicon glyphicon-edit"></span> <span class="edit-link">', '</span></div>' ); ?>

					</div><!-- .entry-meta -->
				<?php endif; ?>
			</header><!-- .entry-header -->

			<?php if ( is_search() ) : // Only display Excerpts for Search ?>
			<div class="entry-summary">
				<?php the_excerpt(); ?>
				<p><a title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" class="btn btn-default read-more" href="<?php the_permalink(); ?>"><?php _e( 'Leer más', 'AlpheratzTheme' ); ?></a></p>
			</div><!-- .entry-summary -->
			<?php else : ?>
				<!-- aquí es pot ficar contingut -->
			<?php endif; ?>
		</div>
</article><!-- #post-## -->
