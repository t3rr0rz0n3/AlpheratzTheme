<?php get_header(); ?>
                            <div id="primary" class="col-md-8 archive" <?php echo nc_sidebarPosition() ?>>
                                <main id="main">
                                    <h2 class="page-title entry-title"><?php single_cat_title( $prefix = '', $display = true ); ?></h2>

                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                        <?php
                                            get_template_part( 'content-archive');
                                        ?>

                                    <?php endwhile; else: ?>
                                        <?php
                                            get_template_part( '404');
                                        ?>

                                    <?php endif; ?>
                                    <nav class="navigation paging-navigation" role="navigation">
                                        <div class="nav-links">
                                            <?php AlpheratzPagination(); ?>
                                        <div><!-- .nav-links -->
                                    </nav><!-- .navigation -->
                                </main>
                            </div><!-- #primary -->

                            <div id="secondary" class="col-md-4">
                                <?php get_sidebar(); ?>
                            </div><!-- #secondary -->
<?php get_footer(); ?>
