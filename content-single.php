<!-- Contenido de la entrada -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="thumb-img thumb-single" style="background-image:url(
		<?php if (get_post_thumbnail_id()) {
			echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		} else {
			echo bloginfo('template_directory') . '/img/default-thumb.png';
		} ?>);">
	</div>

	<div class="post-inner-title">
		<h1 class="post-title"><?php the_title(); ?></h1>
	</div><!-- .post-inner-title -->

	<div class="post-inner-info">
		<div class="entry-meta">
			<ul class="info">
				<li><span class="fa fa-calendar"></span>
					<?php the_time('d/m/Y') ?></li>
				<li><span class="fa fa-user"></span>
					<a title="<?php the_author(); ?>" alt="<?php the_author(); ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?></a>
				</li>
				<li>
					<span class="fa fa-tags"></span>
					<?php
						$category = get_the_category();
						if ($category) {
						  echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "Ver todas las entradas de %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
					  }
					?>
				</li>
				<li>
					<span class="fa fa-comment"></span>
					<?php
	                    comments_number(
	                    '<span>' . __("Ningún","AlpheratzTheme") . '</span> ' . __("comentario","AlpheratzTheme") . '',
	                    '<span>' . __("Un","AlpheratzTheme") . '</span> ' . __("comentario","AlpheratzTheme") . '',
	                    '<span>%</span> ' . __("comentarios","AlpheratzTheme") );?>
	                <?php __("","AlpheratzTheme"); ?>
				</li>
				<li>
					<?php edit_post_link( __( 'Editar', 'AlpheratzTheme' ), '<span class="glyphicon glyphicon-edit"></span> <span class="edit-link">', '</span>' ); ?>
				</li>
				<li>
					<?php nc_breadcrumb(); ?>
				</li>
			</ul><!-- .info -->
		</div><!-- .entry-meta -->
	</div><!-- .post-inner-info -->

	<div class="post-inner-content content-article">
		<header class="entry-header page-header">

		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php oldPosts() ?>
			<?php the_content(); ?>
		</div><!-- .entry-content -->

		<footer class="entry-meta">
			<div id="anuncio" class="container">
				<?php echo of_get_option( 'ads', '#' ); ?>
			</div><!-- #anuncio -->

			<?php if(has_tag()) : ?>
			<!-- tags -->
			<div class="tags-cloud">
				<div class="row">
					<div class="tagcloud">
						<span class="glyphicon glyphicon-tags"></span>
	    				<?php
	    					$tags = get_the_tags(get_the_ID());
	    					foreach($tags as $tag){
	    						echo '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a> ';
	    					}
	    				?>
					</div>
				</div><!-- end tags -->
			</div>
			<?php endif; ?>
			<div class="share-content">
				<?php nc_shareContent(); ?>
			</div>

			<div class="related-posts">
				<div class="row">
					<?php nc_relatedPosts(); ?>
				</div>
			</div>

		</footer><!-- .entry-meta -->
	</div>

	<div class="post-inner-license">
		<p><?php _e('El contenido de esta entrada está bajo licencia Creative Commons', 'AlpheratzTheme'); ?></p>
		<p class="text-center"><?php nc_license(); ?></p>
	</div>

	<div class="post-inner-content secondary-content-box">
		<div class="row">
			<div class="avatar col-md-3">

				<div class="avatar-img">
					<?php echo get_avatar(get_the_author_meta('ID') , '100'); ?>
				</div><!-- .author-img -->

			</div><!-- .avatar -->

			<div class="author-bio content-box-inner col-md-9">

				<h4 class="author-name">
					<a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php echo get_the_author_meta('display_name'); ?></a>
					<span class="glyphicon glyphicon-user"></span>
				</h4>
				<p class="author-description">
	                <?php echo get_the_author_meta('description'); ?>
	            </p>
			</div><!-- .author-bio -->
			<div class="author-social col-md-12">
				<div class="nextandPrevious col-md-6 text-left">
					<ul class="navnap col-md-12">
						<li class="col-md-6 left">
							<?php next_post_link('%link', '<span data-toggle="tooltip" data-placement="top" title="' . __('Entrada anterior', 'AlpheratzTheme') . '" class="glyphicon glyphicon-chevron-left"></span>'); ?>
						</li>

						<li class="col-md-6 right">
							<?php previous_post_link('%link', '<span data-toggle="tooltip" data-placement="top" title="' . __('Siguiente entrada', 'AlpheratzTheme') . '" class="glyphicon glyphicon-chevron-right"></span>'); ?>
						</li>
					</ul>
				</div>
				<div class="list-social col-md-6 text-right">
					<?php alperatzListSocialNetwork(); ?>
				</div><!-- .list-social -->
			</div><!-- .author-social -->
		</div><!-- .row -->
	</div><!-- .post-inner-content -->
</article><!-- #post-## -->
