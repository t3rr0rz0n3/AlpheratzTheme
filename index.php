<?php get_header(); ?>
                            <div id="primary" class="home">
                                <main id="main">
                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                        <?php
                                            get_template_part( 'content');
                                        ?>

                                    <?php endwhile; else: ?>

                                        <div class="">
                                            <h1>
                                                <?php _e('Aún no hay artículos para cargar', 'AlpheratzTheme'); ?>
                                            </h1>
                                        </div>

                                    <?php endif; ?>
                                    <nav class="navigation paging-navigation" role="navigation">
                                        <div class="nav-links">
                                            <?php AlpheratzPagination(); ?>
                                        <div><!-- .nav-links -->
                                    </nav><!-- .navigation -->
                                </main>
                            </div><!-- #primary -->
<?php get_footer(); ?>
