// Search box toggle
$(document).ready(function(){
    $(".buscador").click(function(event){
        $("#box_animate").slideToggle('5000');
        event.preventDefault();
        $("#s").focus();
    });
});

$(document).ready(function(){
    $(".buscador").click(function(){
        $("a").attr("data-toggle");
    });
});

// Tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// popover
$('#cookie').popover()

// Show chevron up (mostrar flecha, ocultar flecha)
$(document).ready(function($){
    $(window).scroll(function () {
        if ( $(this).scrollTop() > 500 )
            $("#backtotop").fadeIn("slow");
        else
            $("#backtotop").fadeOut("slow");
        });

    $("#totop").click(function () {
        $("body,html").animate({ scrollTop: 0 }, 800 );
        return false;
    });
});

// Go to top effect
var toTop;
function toTopEffect() {
    if (document.body.scrollTop != 0 || document.documentElement.scrollTop != 0) {
        window.scrollBy(0, -15);
        toTop = setTimeout('toTopEffect()', 5);
    } else {
        clearTimeout(toTop);
    }
}

// Close cookies
$(document).ready(function(){
    var nombreCookie="PortalLinux-cookie";
    var collaborate="collaborate-cookie";

    if(getCookie(nombreCookie)==""){
        $('.cookies').removeClass("oculto");
        $('.close-capa').dblclick(function(){
            $(this).fadeOut(500);
            createCookie(nombreCookie,1,365);
        });
    };
    if (getCookie(collaborate)=="") {
        $('.collaborate').removeClass("oculto");
        $('.close-panel').click(function(){
            $(this).fadeOut(500);
            createCookie(collaborate,2,30);
        })
    };
});

function createCookie(name,value,days){
    if(days){
        var date=new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires="; expires="+date.toGMTString();
    }else
        var expires="";
        document.cookie=name+"="+value+expires+"; path=/";
}

function readCookie(name){
    var nameEQ=name+"=";
    var ca=document.cookie.split(';');

    for(var i=0;i<ca.length;i++){
        var c=ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1,c.length);
        }

        if (c.indexOf(nameEQ)==0){
            return c.substring(nameEQ.length,c.length);
        }
    }
    return null;
}

function eraseCookie(name){
    createCookie(name,"",-1);
}

function getCookie(cname){
    var name=cname+"=";
    var ca=document.cookie.split(';');

    for(var i=0;i<ca.length;i++){
        var c=ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }

        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return"";
}

/* COLLABORATE */
var target = $("footer.entry-meta").offset().top;

var interval = setInterval(function() {
    if ($(window).scrollTop() >= target) {
        $('.collaborate').removeClass('hidden');
        $('.collaborate').animate({
            right: 0,
            display: "initial"
        });
        //clearInterval(interval);
    } else if ($(window).scrollTop() <= target) {
        $('.collaborate').animate({
            right: -350,
            display: "none"
        });
        //clearInterval(interval);
    }
}, 500);
