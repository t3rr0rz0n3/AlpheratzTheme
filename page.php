<?php get_header(); ?>
                            <div id="primary" class="col-md-8 page" <?php echo nc_sidebarPosition() ?>>
                                <main id="main">
                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                        <?php
                                            get_template_part( 'content-single', get_post_format() );
                                        ?>

                                    <?php endwhile; else: ?>

                                        <?php
                                            get_template_part( '404');
                                        ?>

                                    <?php endif; ?>
                                </main>
                            </div><!-- #primary -->

                            <div id="secondary" class="col-md-4">
                                <?php get_sidebar(); ?>
                            </div><!-- #secondary -->
<?php get_footer(); ?>
