<?php get_header(); ?>
	<div id="primary" class="content-area col-md-8 search" <?php echo nc_sidebarPosition() ?>>
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title entry-title">
					<?php printf( __( 'Resultados para: %s', 'AlpheratzTheme' ), '<span>' . get_search_query() . '</span>' ); ?>
				</h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content-search', 'search' ); ?>

			<?php endwhile; ?>

		<?php else : ?>

			<h1 class="page-title entry-title">
				<?php printf( __( 'Parece que no hemos encontrado nada sobre "%s"', 'AlpheratzTheme' ), '<span>' . get_search_query() . '</span>' ); ?>
			</h1>
			<h2><?php _e('Quizás puedas volver a buscar algo que tengamos:', 'AlpheratzTheme'); ?></h2>

			<?php get_template_part( 'searchform', 'none' ); ?>

		<?php endif; ?>
			<nav class="navigation paging-navigation" role="navigation">
				<div class="nav-links">
					<?php AlpheratzPagination(); ?>
				<div><!-- .nav-links -->
			</nav><!-- .navigation -->
		</main><!-- #main -->
	</div><!-- #primary -->

	<!-- #SECONDARY, la sidebar -->
	<div id="secondary" class="col-md-4">
		<?php get_sidebar(); ?>
	</div><!-- #secondary -->
<?php get_footer(); ?>
