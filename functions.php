<?php

//eliminar codigo basura de cabecera
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

//Automatically move JavaScript code to page footer, speeding up page loading time.

remove_action('wp_head', 'wp_print_scripts');
remove_action('wp_head', 'wp_print_head_scripts', 9);
remove_action('wp_head', 'wp_enqueue_scripts', 1);
add_action('wp_footer', 'wp_print_scripts', 5);
add_action('wp_footer', 'wp_enqueue_scripts', 5);
add_action('wp_footer', 'wp_print_head_scripts', 5);

/* FILES */

// NunkiCore
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/NunkiCore/' );
require_once dirname( __FILE__ ) . '/inc/NunkiCore/options-framework.php';
require_once dirname( __FILE__ ) . '/inc/NunkiCore/nunki-options.php';

// Loads options.php from child or parent theme
$optionsfile = locate_template( 'options.php' );
load_template( $optionsfile );

// Load languages
add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup(){
    load_theme_textdomain('AlpheratzTheme', get_template_directory() . '/languages');
}
/* MAIN FUNCTIONS */

// Register Custom Navigation Walker and add menu
require_once('wp_bootstrap_navwalker.php');

register_nav_menus( array(
    'primary' => __( 'Menu Superior', 'Alpheratz Theme' ),
) );

// Active thumbnails
add_theme_support('post-thumbnails');
add_image_size('list_articles_thumbs', 350, 270, true );
the_post_thumbnail();

the_post_thumbnail('thumbnail');    // Thumbnail (default 150px x 150px max)
the_post_thumbnail('medium');       // Medium resolution (default 300px x 300px max)
the_post_thumbnail('large');        // Large resolution (default 640px x 640px max)
the_post_thumbnail('full');         // Original image resolution (unmodified)

// Add sidebar
register_sidebar(array(
    'name' => 'SideBar',
    'before_widget' => '<div class="widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Add sidebar on footer
register_sidebar(array(
    'name' => 'Footer',
    'before_widget' => '<div class="widget col-md-4">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Add Social Network on profile
function alpheratzAuthorSocialIcons( $contactmethods ) {

    $contactmethods['twitter'] = 'Twitter URL';
    $contactmethods['facebook'] = 'Facebook URL';
    $contactmethods['googleplus'] = 'Google + URL';
    $contactmethods['correo'] = 'Correo Electrónico';
    $contactmethods['github'] = 'GitHub URL';
    /*$contactmethods['gnusocial'] = 'GNU Social URL';
    $contactmethods['diaspora'] = 'Diaspora* URL';*/

    return $contactmethods;
}
add_filter( 'user_contactmethods', 'alpheratzAuthorSocialIcons', 10, 1);

// Social Network in user profile
function alperatzListSocialNetwork() {

    echo '<ul class="icons">';
    $twitter = get_the_author_meta( 'twitter' );
    if ( $twitter && $twitter != '' ) {
        echo '<li><a target="_blank" class="icon-social tw" href="' . esc_url($twitter) . '"><i class="fa fa-twitter"></i></a></li>';
    }

    $facebook = get_the_author_meta( 'facebook' );
    if ( $facebook && $facebook != '' ) {
        echo '<li><a target="_blank" class="icon-social fb" href="' . esc_url($facebook) . '"><i class="fa fa-facebook"></i></a></li>';
    }

    $googleplus = get_the_author_meta( 'googleplus' );
    if ( $googleplus && $googleplus != '' ) {
        echo '<li><a target="_blank" class="icon-social gp" href="' . esc_url($googleplus) . '"><i class="fa fa-google-plus"></i></a></li>';
    }

    $correo = get_the_author_meta( 'correo' );
    if ( $correo && $correo != '' ) {
        echo '<li><a target="_blank" class="icon-social ce" href="mailto:' . esc_url($correo) . '"><i class="fa fa-envelope"></i></a></li>';
    }

    $github = get_the_author_meta( 'github' );
    if ( $github && $github != '' ) {
        echo '<li><a target="_blank" class="icon-social gh" href="' . esc_url($github) . '"><i class="fa fa-github-alt"></i></a></li>';
    }

    /*$gnusocial = get_the_author_meta( 'gnusocial' );
    if ( $gnusocial && $gnusocial != '' ) {
        echo '<li><a target="_blank" class="gs" href="' . esc_url($gnusocial) . '"><span class="sociconoff">GNUSocial</span></a></li>';
    }

    $diaspora = get_the_author_meta( 'diaspora' );
    if ( $diaspora && $diaspora != '' ) {
        echo '<li><a target="_blank" class="dp" href="' . esc_url($diaspora) . '"><span class="sociconoff">Diaspora</span></a></li>';
    }*/
    echo '</ul>';
}

function oldPosts() {

    // Variables, a year, 2 year, 3 year, 4 year, more than 5 years
    $time_old_year = 60*60*24*365;
    $time_old_2year = 60*60*24*365*2;
    $time_old_3year = 60*60*24*365*3;
    $time_old_4year = 60*60*24*365*4;
    $time_old_MoreYear = 60*60*24*365*5;
    $time_now = date('U')-get_the_time('U');

    if ($time_now > $time_old_year && $time_now < $time_old_2year) {
        echo '<div class="alert alert-success" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace un año.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_2year && $time_now < $time_old_3year) {
        echo '<div class="alert alert-info" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace dos años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_3year && $time_now < $time_old_4year) {
        echo '<div class="alert alert-warning" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace tres años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_4year && $time_now < $time_old_MoreYear) {
        echo '<div class="alert alert-danger" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace cuatro años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_MoreYear) {
        echo '<div class="alert alert-black" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace más de cinco años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    }
}

// Posts relacionados (en pruebas)
function AlpheratzRelatedAuthorPosts() {
    global $authordata, $post;
    $authors_posts = get_posts( array( 'author' => $authordata->ID, 'post__not_in' => array( $post->ID ), 'posts_per_page' => 6 ) );
    $output = '<div class="author-box-post text-center">';
    foreach ( $authors_posts as $authors_post ) {
        $output .= '<div class="col-md-6 col-sm-12 col-xs-12 author-box "><h4><a href="' . get_permalink( $authors_post->ID ) . '">' . apply_filters( 'the_title', $authors_post->post_title, $authors_post->ID ) . '</a></h4></div>';
    }
    $output .= '</div>';
    return $output;
}


// Mostrar los posts populares en single-content

function popularPosts() {

    $mipost = $post;
    $args = array( 'numberposts' => 3, 'orderby' => 'rand', 'post_status' => 'publish', 'offset' => 1);
    $rand_posts = get_posts( $args );

    foreach ($rand_posts as $post) {

        $id = $post->ID;
        $title = $post->post_title;
        $titlePrint = substr($title, 0, 26) . "...";

        echo '<li class="trendingPost">';
        echo '<a data-toggle="tooltip" data-placement="bottom" href="' . get_permalink($id) . '" title="' . $title . '">' . $titlePrint . '</a> ';
        echo '</li>';
    }
}

/** THEME OPTIONS FUNCTIONS **/

// Alpheratz Pagination
function AlpheratzPagination() {
    global $wp_query;
    $big = 999999999;
    $pages = paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?page=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'prev_next' => false,
        'type' => 'array',
        'prev_next' => TRUE,
        'prev_text' => '&larr; Previous',
        'next_text' => 'Next &rarr;',
            ));
    if (is_array($pages)) {
        $current_page = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<ul class="pagination">';
        foreach ($pages as $i => $page) {
            if ($current_page == 1 && $i == 0) {
                echo "<li class='active'>$page</li>";
            } else {
                if ($current_page != 1 && $current_page == $i) {
                    echo "<li class='active'>$page</li>";
                } else {
                    echo "<li>$page</li>";
                }
            }
        }
        echo '</ul>';
    }
}

// Alpheratz Comments
function wp_bootstrap_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix comment-body">
			<div class="comment-author vcard clearfix">
				<div class="vcard col-sm-2">
					<?php echo get_avatar( $comment, $size='75' ); ?>
                </div>
				<div class="col-sm-10 comment-text">
					<?php printf('<h4>%s</h4>', get_comment_author_link()) ?>

                    <?php if ($comment->comment_approved == '0') : ?>
       					<div class="alert alert-info" role="alert">
                            <p><?php _e('Tu comentario está en la lista para ser moderado.','AlpheratzTheme') ?></p>
          				</div>
					<?php endif; ?>
                        <?php comment_text() ?>
                        <time datetime="<?php echo comment_time('Y-m-j'); ?>">
                            <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
                                <?php comment_time('j F, Y - H:m'); ?>
                            </a>
                        </time>

					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                    <?php edit_comment_link(__(' Editar '),'<span class="edit-comment"><span class="glyphicon glyphicon-edit"></span>','</span>') ?>

                </div>
			</div>
		</article>
    <!-- </li> is added by wordpress automatically -->
<?php
} // don't remove this bracket!

//Modificar los campos Autor, Email y Sitio web del formulario de comentarios
function apk_modify_comment_fields( $fields ) {

	//Variables necesarias para que esto funcione
    $commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

	$fields =  array(

	  'author' =>
	    '<input class="input-form-comment col-md-4" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
	    '" size="30"' . $aria_req . ' placeholder="' . __('NOMBRE*', 'AlpheratzTheme') . '" />', //Editamos el campo autor

	  'email' =>
	    '<input class="input-form-comment col-md-4 mig-form" id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
	    '" size="30"' . $aria_req . ' placeholder="' . __('EMAIL*', 'AlpheratzTheme') . '" />', //Editamos el campo email

	  'url' =>
	    '<input class="input-form-comment col-md-4" id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
	    '" size="30" placeholder="' . __('WEB', 'AlpheratzTheme') . '"  />', //Editamos el campo sitio web
	);

	return $fields;
}
add_filter('comment_form_default_fields', 'apk_modify_comment_fields');

function wpsites_modify_comment_form_text_area($arg) {
    $arg['comment_field'] = '<textarea class="textarea-comment col-md-12" id="comment" name="comment" cols="45" placeholder="COMENTARIO*" rows="6" aria-required="true"></textarea>';
    return $arg;
}

add_filter('comment_form_defaults', 'wpsites_modify_comment_form_text_area');


add_action( 'optionsframework_custom_scripts', 'optionsframework_custom_scripts' );

function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#slideon').click(function() {
  		jQuery('#section-categoriaslider').fadeToggle(400);
	});

	if (jQuery('#slideon:checked').val() !== undefined) {
		jQuery('#section-categoriaslider').show();
	}

});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#slideon').click(function() {
  		jQuery('#section-interval').fadeToggle(400);
	});

	if (jQuery('#slideon:checked').val() !== undefined) {
		jQuery('#section-interval').show();
	}

});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#slideon').click(function() {
  		jQuery('#section-pauseonhover').fadeToggle(400);
	});

	if (jQuery('#slideon:checked').val() !== undefined) {
		jQuery('#section-pauseonhover').show();
	}

});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#slideon').click(function() {
  		jQuery('#section-numpost').fadeToggle(400);
	});

	if (jQuery('#slideon:checked').val() !== undefined) {
		jQuery('#section-numpost').show();
	}

});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#collaborate').click(function() {
  		jQuery('#section-collaborateurl').fadeToggle(400);
	});

	if (jQuery('#collaborate:checked').val() !== undefined) {
		jQuery('#section-collaborateurl').show();
	}

});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#cookies').click(function() {
  		jQuery('#section-cookieshome').fadeToggle(400);
	});

	if (jQuery('#cookies').val() !== undefined) {
		jQuery('#section-cookieshome').show();
	}

});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#cookies').click(function() {
  		jQuery('#section-cookiesall').fadeToggle(400);
	});

	if (jQuery('#cookies').val() !== undefined) {
		jQuery('#section-cookiesall').show();
	}

});
</script>


<!-- FADE ERROR NOTICE UPDATE NOTICE -->
<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#setting-error-save_options').show().delay(8000).fadeOut('slow');

});
</script>

<?php
}


// END FILE
?>
