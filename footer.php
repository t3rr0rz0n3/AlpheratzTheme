                            </div><!-- #content -->
                        </div><!-- .row -->
                    </div><!-- .container .main-content-area -->
                    <?php nc_collaborate(); ?>
                </div><!-- #content -->
            <div id="footer-area">
                <div class="container footer-inner">
                    <div class="row">
                        <div class="foother-widget-area">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer') ) : endif; ?>
                        </div>
                    </div>
                </div>
                <div id="colophon" class="site-footer">
                    <div class="col-md-4 copyright">
                        <?php nc_toTopPage(); ?>
                        <a title="<?php wp_title(); ?>" alt="" href="<?php echo esc_url( home_url( '/' ) ); ?>" ><?php echo of_get_option( 'copyright', 'Nombre Autor' ); ?> - <?php printf(__('%s','Alpheratz'), date('Y')); ?></a>
                    </div>
                    <div class="col-md-4 col-xs-12 social">
                        <ul class="social-network">
                            <?php nc_SocialNetworkFooter(); ?>
                        </ul>
                    </div>
                    <div class="col-md-4 themeName">
                        <a title="PortalLinux" href="https://gitlab.com/zagur/AlpheratzTheme" target="_blank"><font color="F1F1F1"><strong>Alpheratz</strong></font><strong><span class="second">Theme</span></strong></a> por
                        <a title="Jesús Camacho" href="http://www.entrebits.org">Jesús Camacho</a>
                    </div><!-- .themeName -->
                </div><!-- #colophon .site-footer -->
            </div><!-- #footer-area -->
            <?php nc_christmasTree(); ?>
            <?php nc_cookiesMessage(); ?>


        </main><!-- #page -->

        <!-- Scripts -->
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/alpheratz.min.js" type="text/javascript"></script>

        </script>

        <?php wp_footer(); ?>
    </body>
</html>
