<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-6 col-sm-6 col-xs-12'); ?>>
	<div class="blog-item-wrap square">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
			<div class="img">
				<?php if ( has_post_thumbnail() ) {
					the_post_thumbnail('large', array( 'class' => 'img-responsive' ));
				} else { ?>
						<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/img/default-thumb.png" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
				<?php } ?>
				<h2 class="entry-title">
					<?php the_title(); ?>
				</h2>
			</div> <!-- img -->
		</a>
	</div><!-- square -->

	<div class="post-inner-content">
		<header class="entry-header page-header">

			<?php if ( 'post' == get_post_type() ) : ?>
				<div class="entry-meta row">
					<div class="post_date col-md-6">
                        <span class="glyphicon glyphicon-calendar text-left"></span>
            			<?php the_time('d/m/Y') ?>
					</div>
					<div class="post_categories col-md-6 text-right">
						<span class="glyphicon glyphicon-tag"></span>
						<?php
							$category = get_the_category();
							if ($category) {
							  echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "Ver todas las entradas de %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
							}
						?>
					</div>
					<?php edit_post_link( __( 'Editar', 'AlpheratzTheme' ), '<div class="edit-post"><span class="glyphicon glyphicon-edit"></span> <span class="edit-link">', '</span></div>' ); ?>
				</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->
	</div><!-- .post-inner-content -->
</article><!-- #post-## -->
