<div class="row">
    <div class="share-post">

        <!--<h4><?php _e('Compartir', 'AlpheratzTheme');?></h4>-->
        <ul class="share-buttons">
            <!-- FACEBOOK -->
            <li>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" title="<?php _e('Compartir con Facebook', 'AlpheratzTheme'); ?>" target="_blank">
                    <div class="social social-fb col-md-3 col-sm-3 col-xs-3">
                        <div class="logo col-md-3">
                             <i class="fa fa-facebook"></i>
                        </div>
                        <div class="name col-md-9">
                            <span>Facebook</span>
                        </div>
                    </div>
                </a>
            </li>
            <!-- TWITTER -->
            <li>
                <a href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>%20%23PortalLinux&via=zagurito" target="_blank" title="<?php _e('Compartir con Twitter', 'AlpheratzTheme'); ?>">
                    <div class="social social-tw col-md-3 col-sm-3 col-xs-3">
                        <div class="logo col-md-3">
                             <i class="fa fa-twitter"></i>
                        </div>
                        <div class="name col-md-9">
                            <span>Twitter</span>
                        </div>
                    </div>
                </a>
            </li>
            <!-- Google+ -->
            <li>
                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" title="<?php _e('¡Compartir en Google+!', 'AlpheratzTheme'); ?>">
                    <div class="social social-gp col-md-3 col-sm-3 col-xs-3">
                        <div class="logo col-md-3">
                             <i class="fa fa-google-plus"></i>
                        </div>
                        <div class="name col-md-9">
                            <span>Google+</span>
                        </div>
                    </div>
                </a>
            </li>
            <!-- Email -->
            <li>
                <a href="mailto:?subject=<?php the_title(); ?>&body=<?php the_title(); ?>:<?php the_permalink(); ?>" target="_blank" title="<?php _e('Envia artículo por correo', 'AlpheratzTheme'); ?>">
                    <div class="social social-em col-md-3 col-sm-3 col-xs-3">
                        <div class="logo col-md-3">
                             <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="name col-md-9">
                            <span>E-Mail</span>
                        </div>
                    </div>
                </a>
            </li>
            <!--
            <li class="dropdown">
                <a href="#" class="social social-gp dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <div class="social social-ot col-md-3">
                        <div class="logo col-md-3">
                             <i class="fa fa-google-plus"></i>
                        </div>
                        <div class="name col-md-9">
                            <span>Otros</span>
                        </div>
                    </div>
                </a>
                <ul class="dropdown-menu" aria-labelledby="dropdownmenu1">


                </ul>
            </li>


            <li>
                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" title="Compartir en Google+">
                    <img src="<?php bloginfo('template_url')?>/img/iconsSocial/Google+.svg">
                </a>
            </li>
            <li>
                <a href="http://www.reddit.com/submit?url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" title="Envíar a Reddit">
                    <img src="<?php bloginfo('template_url')?>/img/iconsSocial/Reddit.svg">
                </a>
            </li>-->

        </ul>
    </div> <!-- /. share-post -->
</div><!-- .row -->
