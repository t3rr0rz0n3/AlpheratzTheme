<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post-inner-title">
		<h1 class="post-title author-post-title">
			<?php echo get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name'); ?>
		</h1>
	</div>

	<div class="post-inner-content content-author">
		<div class="row">
			<div class="avatar col-md-3">
				<div class="avatar-img">
					<?php echo get_avatar(get_the_author_meta('ID') , '100'); ?>
				</div><!-- .author-img -->
			</div><!-- .avatar -->
			<div class="content-box-inner col-md-9">
				<p class="author-description"><?php echo get_the_author_meta('description'); ?></p>
			</div><!-- .author-bio -->

			<div class="post-by-author col-md-12">
				<h2>Últimas entradas de <?php echo get_the_author_meta('display_name'); ?></h2>
				<?php echo AlpheratzRelatedAuthorPosts(); ?>
			</div>
		</div><!-- .row -->
	</div>
	<div class="author-social col-md-12">
		<div class="list-social col-md-6 text-right">
			<?php alperatzListSocialNetwork(); ?>
		</div><!-- .list-social -->
	</div><!-- .author-social -->

</article>
